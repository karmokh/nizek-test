module.exports = {
    'tryAgain': 'Please try again later',
    'usernameAndPasswordRequired': 'username and password are required',
    'incorrectCredentials': 'username or password is incorrect',
    'logoutError': 'Error logging out'
}