import {NextFunction, Request, Response} from 'express';

exports.errorHandler = (message: string) => {
    const error = new Error();
    error.message = message;
    throw error;
}

exports.get404 = (req: Request, res: Response) => {
    res.status(404).json({message: "Not Found!"});
}

exports.getErrors = async (error: any, req: Request, res: Response, next: NextFunction) => {
    const status = error.statusCode || 500;
    res.status(status).json({message: error.message});
}

