const express = require('express');
const router = express.Router();

const AuthService = require('./auth.service');

router.get('/', AuthService.login);
router.post('/login', AuthService.authenticate);
router.get('/logout', AuthService.logout);
router.get('/profile', AuthService.profile);

module.exports = router;