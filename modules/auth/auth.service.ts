import {NextFunction, Request, Response} from 'express';
import {User} from "../user/models/user.model";

declare module "express-session" {
    interface SessionData {
        user: { username: string };
    }
}

const bcrypt = require('bcryptjs');
const {errorHandler} = require("../../utils/errorHandler");
const messages = require("../../messages");
const users = require("../../fake-users");

exports.login = (req: Request, res: Response, next: NextFunction) => {
    if (req.session.user) {
        res.redirect('profile')
    } else {
        res.render('auth/login.ejs');
    }
}

exports.authenticate = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const {username, password, rememberMe} = req.body;

        if (!username || !password) {
            return errorHandler(messages.usernameAndPasswordRequired)
        }

        const user = users.find((u: User) => u.username === username);
        if (!user) {
            return errorHandler(messages.incorrectCredentials)
        }

        let passwordCompare = await bcrypt.compare(password, user.password);
        if (!passwordCompare) {
            return errorHandler(messages.incorrectCredentials)
        }

        if (user) {
            delete user.password
            req.session.user = user;

            if (rememberMe) {
                req.session.cookie.maxAge = 30 * 24 * 60 * 60 * 1000;
            }
        }

        res.redirect('/profile')
    } catch (err) {
        next(err)
    }
}

exports.profile = (req: Request, res: Response, next: NextFunction) => {
    if (req.session.user) {
        res.render('auth/profile.ejs', {username: req.session.user.username});
    } else {
        res.redirect('/')
    }
}

exports.logout = (req: Request, res: Response, next: NextFunction) => {
    if (req.session.user) {
        req.session.destroy(err => {
            if (err) {
                throw errorHandler(messages.logoutError);
            }
        });
    }
    res.redirect('/')
}