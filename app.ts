import express, {Express} from "express";
import session from 'express-session';
import dotenv from "dotenv";
import bodyParser from 'body-parser';
import helmet from "helmet";
import hpp from "hpp";
import rateLimit from "express-rate-limit";

const xss = require("xss-clean");

const {get404, getErrors} = require("./utils/errorHandler");
const messages = require("./messages");

dotenv.config();

const app: Express = express();
const port = process.env.PORT || 3000;

app.set('view engine', 'ejs');

app.use(rateLimit({
    windowMs: 1 * 60 * 1000,
    max: 90,
    message: {message: messages.tryAgain},
    standardHeaders: true,
    legacyHeaders: false,
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(hpp());
app.use(xss());
app.use(helmet());

app.use(session({
    secret: process.env.SECRET_KEY || 'my-app-secret-key',
    resave: false,
    saveUninitialized: false
}));

// Routes
app.use("/", require("./modules/auth/auth.controller"));

app.use(getErrors);
app.use(get404);

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});